#!/usr/bin/env python
from datetime import datetime, date, timedelta
from calendar import timegm


def get_dt(dt_str):
    if '-' in dt_str:
        dt_str = dt_str.replace('-', '/')
    if dt_str == ['now'] or dt_str == []:
        return datetime.now()
    elif dt_str == ['today']:
        return date.today()

    if len(dt_str) == 1:
        dt_str = dt_str[0].split(' ')

    if len(dt_str) == 1:
        return datetime.strptime(dt_str[0], '%Y/%m/%d')
    elif len(dt_str) == 2:
        return datetime.strptime(' '.join(dt_str), '%Y/%m/%d %H:%M:%S')
    else:
        raise RuntimeError('Cant figure out your date format.')

def get_ts(dt):
    return timegm(dt.timetuple())

def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--add-hours', '-H', default=0, type=int,
        help='add hours to convert datetime to GMT or equivalent')
    parser.add_argument('--convert', '-c', action='store_true',
                        help='convert unix timestamp to isoformat')
    parser.add_argument('datetime_string', nargs='+',
        help='datetime, in format like 2015/09/01 00:00:00, '
            'or just "now" or "today"')
    args = parser.parse_args()
    if args.convert:
        ts = int(args.datetime_string[0])
        dt = datetime.fromtimestamp(ts)
        print(dt.isoformat())
    else:
        dt = get_dt(args.datetime_string)
        dt = dt + timedelta(hours=args.add_hours)
        ts = get_ts(dt)
        print(ts)

if __name__ == '__main__':
    main()
