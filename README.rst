unixtime
========

easy conversions for unix timestamps

Installation
------------

From the project root directory::

    $ python setup.py install

Usage
-----

Simply run it::

    $ unixtime

Use --help/-h to view info on the arguments::

    $ unixtime --help

Release Notes
-------------

:0.0.1:
    Project created
